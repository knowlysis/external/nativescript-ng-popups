export declare class PopupService {
    constructor();
    action(message: string, cancelButtonText: string, actions: string[]): Promise<string>;
}