import { Injectable, Inject } from '@angular/core';
import { MatBottomSheet, MAT_BOTTOM_SHEET_DATA } from '@angular/material'

@Injectable()
export class PopupService {

  constructor(private bottomSheet: MatBottomSheet) { }

  action(message: string, cancelButtonText: string, actions: string[]): Promise<string> {
    this.bottomSheet.open(ActionComponent, {
      data: {message, cancelButtonText, actions}
    });
    return Promise.resolve('W');
  }
}

import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'popups-action',
  template: `<mat-nav-list>
  <span>{{message}}</span>
  <a *ngFor="let action of actions" mat-list-item (click)="actionSelected(action)">
      <span mat-line>{{action}}</span>
  </a>
  <a mat-list-item (click)="actionSelected('cancel')">
      <span mat-line>{{cancelButtonText}}</span>
  </a>
</mat-nav-list>`
})
export class ActionComponent {
  message: string;
  cancelButtonText: string;
  actions: string[];

  constructor(private bottomSheetRef: MatBottomSheetRef<ActionComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) private data: any) {
    this.message = this.data.message;
    this.cancelButtonText = this.data.cancelButtonText;
    this.actions = this.data.actions;
  }

  actionSelected(selectedActionName: string) {
    console.log('You selected ' + selectedActionName);
    this.bottomSheetRef.dismiss();
  }

}
