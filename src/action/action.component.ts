import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';

@Component({
  selector: 'popups-action',
  templateUrl: 'action.component.html'
})
export class ActionComponent {
  message = 'Select an option';
  cancelButtonText = 'Quit';
  actions = [
    'Charmander',
    'Squirtle',
    'Bulbasaur'
  ];

  constructor(private bottomSheetRef: MatBottomSheetRef<ActionComponent>) { }

  actionSelected(selectedActionName: string) {
    console.log('You selected ' + selectedActionName);
    this.bottomSheetRef.dismiss();
  }

}
