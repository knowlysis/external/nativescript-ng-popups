import { Injectable } from '@angular/core';
import { action } from 'tns-core-modules/ui/dialogs';

@Injectable()
export class PopupService {
  constructor() { }

  action(message: string, cancelButtonText: string, actions: string[]): Promise<string> {
    return action(
      message,
      cancelButtonText,
      actions
    );
  }
}