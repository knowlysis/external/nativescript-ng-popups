import { NgModule } from '@angular/core';
import { PopupService, ActionComponent } from './popup.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule, MatBottomSheetModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material';

@NgModule({
  imports: [
    MatBottomSheetModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatListModule
  ],
  declarations: [ActionComponent],
  entryComponents: [ActionComponent],
  providers: [
    PopupService,
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } }
  ],
})
export class PopupModule { }
