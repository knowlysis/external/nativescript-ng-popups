import { NgModule } from '@angular/core';
import { PopupService } from './popup.service';

@NgModule({
  providers: [PopupService],
})
export class PopupModule { }
