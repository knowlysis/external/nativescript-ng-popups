# NativeScript NG Popups

![Short demo of NativeScript NG Popups in action](images/demo.gif)


## Description

> **This is a NativeScript plugin designed specifically for NativeScript [code-sharing](https://docs.nativescript.org/angular/code-sharing/intro#introduction) projects. In addition to code-sharing projects, you can use this plugin in a NativeScript Angular or Angular web application by itself. Other flavors of NativeScript are not supported.**

This plugin provides consistent popup behavior between native and web builds in a NativeScript code-sharing project without using any extra code or [code splitting](https://docs.nativescript.org/angular/code-sharing/code-splitting) on your part. This speeds up development considerably and is something we personally use here at Microexcel CSD throughout many of our projects.


## Installation

From the root folder of your code-sharing or NativeScript Angular project, execute the following command:
```javascript
tns plugin add @microexcel-csd/nativescript-ng-popups
```

Alternatively, if you plan on using this plugin in a web Angular project only, use:
```javascript
npm i @microexcel-csd/nativescript-ng-popups
```


## Usage


#### Support Our Packages

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LM74WLHTJN8BA)