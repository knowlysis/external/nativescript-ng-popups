// import { PopupModule } from '@microexcel-csd/nativescript-ng-popups';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

export const componentDeclarations = [
  AppComponent,
  HomeComponent
];

export const imports = [
  // PopupModule
];

export const routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: HomeComponent,
  },
];
