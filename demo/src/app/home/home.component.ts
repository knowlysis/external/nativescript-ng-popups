import { Component, OnInit } from '@angular/core';
// import { PopupService } from '@microexcel-csd/nativescript-ng-popups';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title = 'NativeScript NG Popups';

  actionBtnText = 'Show Action';
  alertBtnText = 'Show Alert';
  modalBtnText = 'Open Modal';
  promptBtnText = 'Prompt User';
  toastBtnText = 'Show Toast';

  constructor(
    // private popupService: PopupService
    ) { }

  ngOnInit() {}

  action(): void {
    // this.popupService.action('Which Pokémon would you like?', 'Nevermind', [
    //   'Bulbasaur',
    //   'Charmander',
    //   'Squirtle'
    // ]).then((selectedPkmn) => {
    //   console.log(selectedPkmn + ' is a great choice!');
    // });

  }

  alert(): void {

  }

  modal(): void {

  }

  prompt(): void {

  }

  toast(): void {

  }
}
