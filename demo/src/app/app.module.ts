import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from '@src/app/app.component';
import { imports, componentDeclarations } from './app.common';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    ...componentDeclarations
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ...imports
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
